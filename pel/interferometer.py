# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

"""
Tango attribute as a counter

YAML_ configuration example:

.. code-block:: yaml
    - class: interferometer
      package: pel.interferometer
      name: interferometer
      trigger_device:
          type: opiom
          name: $OPIOM_NAME
          counter_number: 1     # CNT 1
"""
import gevent
from bliss.common.measurement import IntegratingCounter
from bliss.config.settings import HashSetting
from bliss.scanning.chain import AcquisitionMaster

class Interferometer(IntegratingCounter):
    def __init__(self, name, config):
        IntegratingCounter.__init__(self, name, None, self)
        trig_config = config['trigger_device']
        self._opiom_card = trig_config.get('name')
        self._opiom_counter = trig_config.\
                              get('counter_number', 1) # default 1
        high_time = trig_config.get('high_time', 1)
        low_time = trig_config.get('low_time', 1)
        nb_pulses = trig_config.get('nb_pulses', 1)
        self.__interfometer_clock = config.get('interfometer_clock', 2000000)
        self._counter_config = HashSetting('%s:counter_config' % self.name,
                                           default_values={'high_time' : high_time,
                                                           'low_time' : low_time,
                                                           'nb_pulses' : nb_pulses})
    @property
    def clock(self):
        return self.__interfometer_clock
    
    @property
    def high_time(self):
        """
        High time in second for the pulse
        """
        return self._counter_config['high_time']
    @high_time.setter
    def high_time(self, value):
        self._counter_config['high_time'] = value

    @property
    def low_time(self):
        """
        Low time in second for the pulse
        """
        return self._counter_config['low_time']
    @low_time.setter
    def low_time(self, value):
        self._counter_config['low_time'] = value

    @property
    def nb_pulses(self):
        """
        Nb pulses generated when trigged
        """
        return self._counter_config['nb_pulses']
    @nb_pulses.setter
    def nb_pulses(self, value):
        self._counter_config['nb_pulses'] = value

    def get_value(self, from_index=0):
        return [self.nb_pulses]

    def create_acquisition_master(self, scan_pars):
        npoints = scan_pars.get('npoints', 1)
        count_time = scan_pars.get('count_time', 1)

        return _InterferometerAcqMaster(self, self.name,
                                        npoints=npoints,
                                        count_time=count_time)

    def __repr__(self):
        params = ' '.join(['%s=%s' % (k, v) for k, v in
                           self._counter_config.iteritems()])
        return "Interferometer %s: %s" % (self.name, params)

class _InterferometerAcqMaster(AcquisitionMaster):
    def __init__(self, device, name, npoints=None, count_time=None):
        AcquisitionMaster.__init__(self, device, name, npoints=npoints,
                                   prepare_once=True, start_once=True)
        self._count_time = count_time

    def prepare(self):
        opiom = self.device._opiom_card
        counter = self.device._opiom_counter
        high_time = self.device.high_time * self.device.clock
        low_time = self.device.low_time * self.device.clock
        nb_pulses = self.device.nb_pulses
        opiom.comm("CNT %d RESET" % counter)
        opiom.comm("CNT %d CLK2 PULSE %d %d %d" %\
                   (counter, high_time, low_time, nb_pulses))

    def start(self):
        if not self.parent:
            self.trigger()

    def stop(self):
        opiom = self.device._opiom_card
        counter = self.device._opiom_counter
        opiom.comm("#CNT %d STOP" % counter)

    def trigger(self):
        self.trigger_slaves()
        opiom = self.device._opiom_card
        counter = self.device._opiom_counter
        opiom.comm("CNT %d START" % counter)

    def wait_ready(self):
        opiom = self.device._opiom_card
        counter = self.device._opiom_counter
        comm = "?SCNT %d" % counter
        while True:
            running = opiom.comm(comm) == "RUN"
            if not running:
                break
            gevent.idle()


