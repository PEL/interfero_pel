import os
from setuptools import setup

setup(
    name = "pel",
    version = "0.0.4",
    author = "PEL",
    author_email = "pel@esrf.fr",
    description = ("Local PEL stuff"),
    license = "BSD",
    keywords = "PEL",
    packages=['pel'],
)
